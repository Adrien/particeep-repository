@echo off
echo 1. build the play module with 'play build-module'
echo 2. copy jar to ./dist
echo 3. copy set up var to build maven archetype
echo 4. launch this
echo add https://bitbucket.org/Adrien/particeep-repository/raw/master/repository/ThemeModule/ThemeModule/1.4 as your repo in dependencies
echo ---

SET jarPath=./lib/api-lib_2.11-1.6.3.jar
SET groupID=api-lib
SET artifactId=api-lib
SET version=1.6.3

# SET jarPath=./lib/webdrive.jar
# SET groupID=webdrive
# SET artifactId=webdrive
# SET version=0.3

# SET jarPath=./lib/play-ThemeModule.jar
# SET groupID=ThemeModule
# SET artifactId=ThemeModule
# SET version=1.9.7

echo mvn install:install-file -DgroupId=%groupID% -DartifactId=%artifactId% -Dversion=%version% -Dfile=%jarPath% -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
call mvn install:install-file -DgroupId=%groupID% -DartifactId=%artifactId% -Dversion=%version% -Dfile=%jarPath% -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
