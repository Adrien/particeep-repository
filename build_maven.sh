#!/bin/bash

# launch with : sh build_maven.sh

jarPath='./lib/play-ThemeModule.jar'
groupID='ThemeModule'
artifactId='ThemeModule'
version='1.10.3'

echo mvn install:install-file -DgroupId=%groupID% -DartifactId=%artifactId% -Dversion=%version% -Dfile=%jarPath% -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
mvn install:install-file -DgroupId=$groupID -DartifactId=$artifactId -Dversion=$version -Dfile=$jarPath -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true


# SET jarPath=./lib/api-lib_2.11-1.3.8.jar
# SET groupID=api-lib
# SET artifactId=api-lib
# SET version=1.3.8

# jarPath='./lib/gdata-spreadsheet-3.0.jar'
# groupID='google'
# artifactId='gdata-spreadsheet'
# version='3.0'
# mvn install:install-file -DgroupId=$groupID -DartifactId=$artifactId -Dversion=$version -Dfile=$jarPath -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
# 
# jarPath='./lib/gdata-base-1.0.jar'
# groupID='google'
# artifactId='gdata-base'
# version='1.0'
# mvn install:install-file -DgroupId=$groupID -DartifactId=$artifactId -Dversion=$version -Dfile=$jarPath -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
# 
# jarPath='./lib/gdata-client-1.0.jar'
# groupID='google'
# artifactId='gdata-client'
# version='1.0'
# mvn install:install-file -DgroupId=$groupID -DartifactId=$artifactId -Dversion=$version -Dfile=$jarPath -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
# 
# jarPath='./lib/gdata-client-meta-1.0.jar'
# groupID='google'
# artifactId='gdata-client-meta'
# version='1.0'
# mvn install:install-file -DgroupId=$groupID -DartifactId=$artifactId -Dversion=$version -Dfile=$jarPath -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
# 
# 
# jarPath='./lib/gdata-core-1.0.jar'
# groupID='google'
# artifactId='gdata-core'
# version='1.0'
# mvn install:install-file -DgroupId=$groupID -DartifactId=$artifactId -Dversion=$version -Dfile=$jarPath -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true

# gdata-base-1.0.jar
# gdata-client-1.0.jar
# gdata-client-meta-1.0.jar
# gdata-core-1.0.jar
# gdata-spreadsheet-3.0.jar
# gdata-spreadsheet-meta-3.0.jar